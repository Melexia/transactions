//
//  ASDF.swift
//  transactiontest
//
//  Created by Andrea Sánchez Román on 31/05/20.
//  Copyright © 2020 Andrea Sánchez Román. All rights reserved.
//

import Foundation

extension NumberFormatter {

    static let currencyFormat: NumberFormatter = {
        let numberFormatter = NumberFormatter()
        numberFormatter.maximumFractionDigits = 2
        numberFormatter.minimum = 2
        numberFormatter.locale = Locale(identifier: "es_ES")
        numberFormatter.numberStyle = .currency
        return numberFormatter
    }()
}

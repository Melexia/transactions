//
//  ApiClient.swift
//  transactiontest
//
//  Created by Andrea Sánchez Román on 31/05/20.
//  Copyright © 2020 Andrea Sánchez Román. All rights reserved.
//

import Foundation
import Alamofire

class ApiClient {

    let sessionManager = Session()

    func request<T: Decodable>(router: NetworkRouter, responseDecoder: ObjectDecoder<T>, completion: @escaping (Result<T, Error>) -> Void) {
        AF.request(router, method: router.method, parameters: router.parameters, encoding: router.parametersEncoding, headers: router.headers).responseData { (response) in
            switch response.result {
            case .success(let value):
                completion(responseDecoder.decode(data: value))

            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
}

///Decode the Data into a Decodable object or an error
public struct ObjectDecoder<T: Decodable> {
    public func decode(data: Data) -> Result<T, Error> {
        do {
            let decodeResult = try JSONDecoder().decode(T.self, from: data)
            return .success(decodeResult)

        } catch {
            return .failure(error)
        }
    }
}

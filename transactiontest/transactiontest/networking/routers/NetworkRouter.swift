//
//  NetworkRouter.swift
//  transactiontest
//
//  Created by Andrea Sánchez Román on 31/05/20.
//  Copyright © 2020 Andrea Sánchez Román. All rights reserved.
//

import Foundation
import Alamofire

public protocol NetworkRouter: Alamofire.URLRequestConvertible, URLConvertible {
    //Base url to communicate with the backend
    var baseURLString: String { get }

    //Type of HTTP method used with the request
    var method: Alamofire.HTTPMethod { get }

    //Endpoint path url
    var path: String { get }

    // Parameters used on the request
    var parameters: [String: Any]? { get }

    // A type used to define how a set of parameters are applied to a `URLRequest`
    var parametersEncoding: ParameterEncoding { get }

    // Extra headers that can be applied to a specific request
    var headers: HTTPHeaders? { get }
}

public extension NetworkRouter {

    var baseURLString: String {
        // Get the url base on a environment or a default value
        return "https://code-challenge-e9f47.web.app/"
    }

    var parametersEncoding: ParameterEncoding {
        return URLEncoding.default
    }

    var headers: HTTPHeaders? {
        // we can add here some validation when the header need to put some extra information. For example when the user has a token to manage his session.
        return nil
    }

    /// Returns a URL request using the information on the NetworkRouter or throws if an `Error` was encountered.
    ///
    /// - throws: An error if the underlying request is nil.
    ///
    /// - returns: A URL request.
    func asURLRequest() throws -> URLRequest {
        let url = try self.baseURLString.asURL()

        var urlRequest = URLRequest(url: url.appendingPathComponent(self.path))
        urlRequest.httpMethod = self.method.rawValue
        return try self.parametersEncoding.encode(urlRequest, with: self.parameters)
    }

    /// Returns a URL using the NetworkRouter info or throws an `Error`.
    ///
    /// - throws: An `Error` if the type cannot be converted to a `URL`.
    ///
    /// - returns: A URL or throws an `Error`.
    func asURL() throws -> URL {
        let url = try self.baseURLString.asURL()
        return url.appendingPathComponent(self.path)
    }
}

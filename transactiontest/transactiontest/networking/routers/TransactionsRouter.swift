//
//  TransactionsRouter.swift
//  transactiontest
//
//  Created by Andrea Sánchez Román on 31/05/20.
//  Copyright © 2020 Andrea Sánchez Román. All rights reserved.
//

import Foundation
import Alamofire

enum TransactionsRouter: NetworkRouter {

    case fetchTransactions

    var method: HTTPMethod {
        return .get
    }

    var path: String {
        return "transactions.json"
    }

    var parameters: [String : Any]? {
        return nil
    }

}

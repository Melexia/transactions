//
//  TransactionsDataModel.swift
//  transactiontest
//
//  Created by Andrea Sánchez Román on 31/05/20.
//  Copyright © 2020 Andrea Sánchez Román. All rights reserved.
//

import Foundation

// MARK: - WelcomeElement
struct TransactionDataModel: Codable {

    // MARK: - Properties
    let id: Int
    let date: Date?
    let amount: Double
    let fee: Double?
    let description: String?
    var total: Double {
        return amount + (fee ?? 0)
    }

    enum CodingKeys: String, CodingKey {
        case id, date, amount, fee, description
    }

    // MARK: - Custom decodable
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decode(Int.self, forKey: .id)
        self.amount = try container.decode(Double.self, forKey: .amount)
        self.fee = try container.decodeIfPresent(Double.self, forKey: .fee)
        self.description = try container.decodeIfPresent(String.self, forKey: .description)

        if let dateString = try container.decodeIfPresent(String.self, forKey: .date), let date = DateFormatter.apiDateFormatter.date(from: dateString) {
            self.date = date
        } else {
            self.date = nil
        }
    }
}

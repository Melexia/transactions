//
//  TransactionTableViewCell.swift
//  transactiontest
//
//  Created by Andrea Sánchez Román on 31/05/20.
//  Copyright © 2020 Andrea Sánchez Román. All rights reserved.
//

import UIKit

class TransactionTableViewCell: UITableViewCell {

    // MARK: - Constants
    static let identifier = String(describing: TransactionTableViewCell.self)

    // MARK: - Outlet
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var feeLabel: UILabel!
    @IBOutlet weak var totalLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        self.containerView.layer.cornerRadius = 4
    }

    // swiftlint:disable:next function_parameter_count
    func update(description: String, amount: String, fee: String, total: String, totalColor: UIColor, date: String) {
        self.descriptionLabel.text = description
        self.dateLabel.text = date

        self.amountLabel.text = amount
        self.feeLabel.text = fee
        self.totalLabel.text = total
        self.totalLabel.textColor = totalColor
    }
}

//
//  TransactionsWorker.swift
//  transactiontest
//
//  Created by Andrea Sánchez Román on 31/05/20.
//  Copyright © 2020 Andrea Sánchez Román. All rights reserved.
//

import Foundation

class TransactionsWorker {

    // MARK: - Properties
    private var client: ApiClient

    // MARK: - Initializers
    init() {
        self.client = ApiClient()
    }

    // MARK: - Methods
    func fetchTransactions(completion: @escaping (Result<[TransactionDataModel], Error>) -> Void) {
        let transactionRouter = TransactionsRouter.fetchTransactions
        
        self.client.request(router: transactionRouter, responseDecoder: ObjectDecoder<[TransactionDataModel]>()) { result in

            switch result {
            case .success(let transactions):
                completion(.success(transactions))

            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
}

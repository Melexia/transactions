//
//  TransactionsViewModel.swift
//  transactiontest
//
//  Created by Andrea Sánchez Román on 03/06/20.
//  Copyright © 2020 Andrea Sánchez Román. All rights reserved.
//

import Foundation

class TransactionsViewModel {

    // MARK: - Properties
    var transactionsOrdered: [TransactionDataModel] {
        return transactions.sorted { $0.date! > $1.date! }
    }
    var transactions = [TransactionDataModel]()

    var worker = TransactionsWorker()

    func fetchTransactions(completion: @escaping (Result<Bool, Error>) -> Void) {
        self.worker.fetchTransactions { result in
            switch result {
            case .success(let transactions):
                // Ignoring transactions with invalid dates
                let transactionsWithValidDates = transactions.filter { $0.date != nil }

                // Validate the transactions id is unique and it is the more recent
                var uniqueTransactions = [TransactionDataModel]()
                transactionsWithValidDates.forEach { transaction in
                    let indexFound = uniqueTransactions.firstIndex { $0.id == transaction.id }

                    if let indexFound = indexFound {
                        if  transaction.date! > uniqueTransactions[indexFound].date! {
                            uniqueTransactions[indexFound] = transaction
                        }

                    } else {
                        uniqueTransactions.append(transaction)
                    }
                }

                self.transactions = uniqueTransactions
                completion(.success(true))

            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
}

//
//  ViewController.swift
//  transactiontest
//
//  Created by Andrea Sánchez Román on 31/05/20.
//  Copyright © 2020 Andrea Sánchez Román. All rights reserved.
//

import UIKit

class TransactionsViewController: UIViewController {

    // MARK: - Outlets
    @IBOutlet private(set) weak var transactionsTableView: UITableView!

    // MARK: - Properties
    var viewModel: TransactionsViewModel?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
        self.fetchTransactions()
    }

    // MARK: - Setup
    private func setup() {
        self.viewModel = TransactionsViewModel()
        self.setupUI()
    }

    private func setupUI() {
        self.title = LocalizedKeys.Transactions.title
        self.setupTableView()
    }

    private func setupTableView() {
        self.transactionsTableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 16, right: 0)
        self.transactionsTableView.scrollIndicatorInsets = UIEdgeInsets(top: 0, left: 0, bottom: 16, right: 0)
        self.transactionsTableView.register(UINib(nibName: TransactionTableViewCell.identifier, bundle: nil), forCellReuseIdentifier: TransactionTableViewCell.identifier)

        self.transactionsTableView.rowHeight = 80
        self.transactionsTableView.estimatedRowHeight = UITableView.automaticDimension
    }

    // MARK: - Fetches
    private func fetchTransactions() {

        self.viewModel?.fetchTransactions { result in
            switch result {
            case .success:
                self.transactionsTableView.reloadData()

            case .failure(let error):
                let alertViewController = UIAlertController(title: LocalizedKeys.errorTitle, message: error.localizedDescription, preferredStyle: .alert)
                self.present(alertViewController, animated: true, completion: nil)
            }
        }
    }
}

// MARK: - UITableViewDataSource
extension TransactionsViewController: UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.viewModel?.transactionsOrdered.count ?? 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: TransactionTableViewCell.identifier) as? TransactionTableViewCell else {
            return UITableViewCell()
        }

        if let transaction = self.viewModel?.transactionsOrdered[indexPath.row] {

            let totalColor = transaction.amount >= 0 ? UIColor.green : UIColor.red

            // Formatting cell text
            let amountFormatted = NumberFormatter.currencyFormat.string(from: NSNumber(value: transaction.amount)) ?? ""
            let feeFormatted = "\(LocalizedKeys.Transactions.fee): " + (NumberFormatter.currencyFormat.string(from: NSNumber(value: transaction.fee ?? 0)) ?? "")
            let totalFormatted = "\(LocalizedKeys.Transactions.total): " + (NumberFormatter.currencyFormat.string(from: NSNumber(value: transaction.total)) ?? "")
            let dateFormatted = DateFormatter.listDateFormatter.string(from: transaction.date!)

            cell.update(description: transaction.description ?? "", amount: amountFormatted, fee: feeFormatted, total: totalFormatted, totalColor: totalColor, date: dateFormatted)
        }

        return cell
    }
}

// MARK: - UITableViewDelegate
extension TransactionsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        UITableView.automaticDimension
    }
}
